# dotsync-service

## Summary
This is the background service project for the dotsync system. The service is what actually performs the synchronization of your dotfiles. gRPC communication is used to communicate between the cli and between the api server. This software will be downloaded and managed by the cli and should only be downloaded if you intend to work on development for it.

## Project Status
This project is in the very early stages of development and is not actively being worked on.

## Installation
This software will be downloaded and managed by the cli and should only be downloaded if you intend to work on development for it.
If you wish to install the source for development purposes then follow the instructions below.

## How to setup development
In order to setup your machine for development and start tweaking the tool follows these instructions.

### Install system dependencies
#### Rust version 1.62
install rust either indivdually or through a version manager like asdf. [asdf-vm](https://asdf-vm.com/)
This project was written with rust version 1.62 but it should work with newer versions as well.

### Clone the repo (or fork!)
```
git clone https://gitlab.com/dotsync/dotsync-service.git
cd dotsync-service
```

### Start coding!
Now that you have rust and the source code you can start tweaking!
## How to contribute
This software system is fully open source and public. In order to contribute to the development follow the below setup instructions.
Once you have your machine setup and ready pick up an issue and start working!

### Branching convention
For every ticket go ahead and make a branch off of main with the following naming convention:
- For features: feat/[ticket-number]-[ticket-name-dashed-like-this]
- For bug fixes: bug/[ticket-number]-[ticket-name-dashed-like-this]

### Merge Requests
Once you're branch is ready to be merged into main go ahead and squash your commits, update your branch off of main, and make a merge request!
Make sure to include your ticket name and number in the title. Also, write up a good description of the work you did and any important notes in the merge request description. Don't forget to link the issue with the merge request too!

## Testing
All code in this project should be unit (and integration where applicable-- see below!) tested. The CI pipeline is configured to
run automated testing for function, documentation, and formatting. Before making a merge request make sure to run your tests locally, if they fail then your merge requested won't be accepted!

### Integartion Testing
This project does not require any integration testing currently. This is subject to change.
