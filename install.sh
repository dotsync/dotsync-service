#!/bin/sh

echo Checking for user..
if id 'dotsync' &>/dev/null; then
    echo 'Dotsync user found.'
else
    echo 'Dotsync user not found. Creating..'
    if [ !$(getent group dotsync) ]; then
        echo 'Dotsync group not found. Creating..'
        groupadd dotsync
    fi
    useradd dotsync -g dotsync
fi
echo 'User and group setup'
echo 'Checking for service..'
if [ -f "/etc/systemd/system/dotsync.service" ]; then
    echo 'Service file found.'
else
    echo 'Service file not found. Creating..'
    sudo cp 'install/dotsync.service' '/etc/systemd/system/dotsync.service'
    sudo systemctl daemon-reload
fi
cd dotsync-background-service
echo 'Building..'
cargo build
echo 'Stopping service..'
sudo systemctl stop dotsync.service
echo 'Copying..'
sudo cp target/debug/dotsync-background-service /usr/sbin/dotsync-service
if [ -d '/usr/share/dotsync' ]; then
    echo 'Working Directory already exists.'
else
    echo 'Working Directory missing. Creating..'
    sudo mkdir '/usr/share/dotsync'
fi
echo 'Starting service..'
sudo systemctl start dotsync.service
echo 'Getting status..'
sudo systemctl status dotsync.service
