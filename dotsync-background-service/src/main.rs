use std::{fs::File, thread, io::Write};
use log4rs::{append::{file::FileAppender, rolling_file::{RollingFileAppender, policy::compound::{CompoundPolicy, trigger::size::SizeTrigger, roll::fixed_window::FixedWindowRoller}}}, Config, config::{Appender, Root}, encode::pattern::PatternEncoder};
use log::{LevelFilter, info, debug, warn, error};
use notify::{Event, RecommendedWatcher, RecursiveMode, Watcher};
use std::sync::mpsc::channel;

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    init_logging();

    info!("-----Booted-----");

    info!("Dotsync service started.");

    info!("-----Starting Loop-----");

    info!("Setting up watcher");
    let watch_path = "/tmp/dotsync-test-file.txt";

    let (mut tx, rx) = channel(1);
    let watcher = RecommendedWatcher::new(move |res| {
        tx.send(res).await.unwrap();
    })?;

    watcher.watch(watch_path.as_ref(), RecursiveMode::Recursive)?;

    while let Some(res) = rx.next().await {
        match res {
            Ok(event) => debug!("Changed: {:?}", event),
            Err(e) => error!("Watch error: {:?}", e)
        }
    }

    loop {
        info!("Sleeping..");
        thread::sleep_ms(500);
    }
}

fn init_logging() {
    let size_limit = 500 * 1024; // 100Kb
    let size_trigger = SizeTrigger::new(size_limit);

    let window_size = 30;
    let fixed_window_roller = FixedWindowRoller::builder().build("/var/log/dotsync/dotsync{}.log", window_size).unwrap();

    let compound_policy = CompoundPolicy::new(Box::new(size_trigger), Box::new(fixed_window_roller));

    let logfile = RollingFileAppender::builder()
        .encoder(Box::new(PatternEncoder::new("[{d}] [{l}] - {m}{n}")))
        .build("/var/log/dotsync/dotsync_active.log", Box::new(compound_policy)).unwrap();

    let config = Config::builder()
        .appender(Appender::builder().build("/var/log/dotsync/dotsync_active.log", Box::new(logfile)))
        .build(Root::builder()
               .appender("/var/log/dotsync/dotsync_active.log")
               .build(LevelFilter::Info)).unwrap();

    log4rs::init_config(config).unwrap();
}
